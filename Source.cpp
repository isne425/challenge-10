#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	List x;
	int option, data;
	do
	{
		cout << "==================================================================";
		cout << "\n1) Head push." << endl;
		cout << "2) Tail push." << endl;
		cout << "3) Head pop." << endl;
		cout << "4) Tail pop." << endl;
		cout << "5) Delete node." << endl;
		cout << "6) Finding a data." << endl;
		cout << "\nChoose an option [Press 0 to end.] : ";
		cin >> option;

		switch (option)
		{
		case 1:
			cout << "Enter your data : ";
			cin >> data;
			x.headPush(data);
			cout << "This is your list : ";
			x.Cout();
			cout << endl;
			break;
		case 2:
			cout << "Enter your data : ";
			cin >> data;
			x.tailPush(data);
			cout << "This is your list : ";
			x.Cout();
			cout << endl;
			break;
		case 3:
			x.headPop();
			cout << "This is your list : ";
			x.Cout();
			cout << endl;
			break;
		case 4:
			x.tailPop();
			cout << "This is your list : ";
			x.Cout();
			cout << endl;
			break;
		case 5:
			cout << "Which node do you want to delete? : ";
			cin >> data;
			x.deleteNode(data);
			break;
		case 6:
			cout << "What is your data? : ";
			cin >> data;
			x.isInList(data);
			if (true) { cout << data << " is in the list."; }
			else { cout << data << "is not in the list."; }
			cout << endl;
			break;
		}
	} while (option != 0);

	cout << "This is your list : ";
	x.Cout();
	cout << endl;
	cout << "==================================================================\n";
	system("Pause");
}