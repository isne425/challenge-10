#include <iostream>
#include "list.h"
using namespace std;

List::~List()
{
	for (Node *p; !isEmpty(); )
	{
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int a)
{
	Node *newnode = new Node(a);
	Node *prenode = head;

	if (!isEmpty()) 
	{
		newnode->next = head;
		head = newnode;
		prenode->prev = head;
	}
	else // if empty information  
	{ 
		head = newnode;
		tail = newnode;
	}
}

void List::tailPush(int b) //Add element to tail of list
{
	Node *newnode = new Node(b);
	Node *prenode = head;

	if (!isEmpty()) 
	{
		tail->next = newnode;
		tail = newnode;

		while (prenode->next->next != NULL)
		{
			prenode = prenode->next; //reset previous node = next previous node
		}
		prenode->prev = tail;
	}
	else 
	{
		head = newnode;// if empty information 
		tail = newnode;
	}
}

int List::headPop() //Remove and return element from front of list
{
	Node *newnode = head;
	int a;//declare integer for get value of return
	if (isEmpty()) { return NULL; }// if empty information 
	if (!isEmpty() && head != tail) // case that empty element
	{
		a = head->info;
		newnode = newnode->next;
		delete head;
		head = newnode;
		head->prev = NULL;
		return a;
	}
	else if (!isEmpty() && head == tail)
	{
		a = newnode->info;
		head = newnode->next;
		delete newnode;
		return a;
	}
	return NULL;
}

int List::tailPop()
{
	int t = tail->info;
	if (head == tail) {
		delete tail;
		head = tail = 0;
	}
	else
	{
		tail = tail->prev;
		delete tail->next;
		tail->next = 0;
	}
	return t;
}

void List::deleteNode(int n)
{
	Node *x = head, *y = head;
	while (head != 0)
	{
		if (x->info == n)
		{
			if (x == head&&head != tail)
			{
				head = head->next;
				cout << "deleted" << endl;
				break;
			}
			else if (head == tail)
			{
				head = 0;
				tail = 0;
				cout << "deleted" << endl;
				break;
			}
			else if (x == tail)
			{
				tail = y;
				tail->next = 0;
				delete x;
				cout << "deleted" << endl;
				break;
			}
			else
			{
				y->next = x->next;
				cout << "deleted" << endl;
				break;
			}
		}

		Node *tmp;
		tmp = head;
		while (tmp != 0) 
		{
			if (tmp->info != n) 
			{
				tmp = tmp->next;
			}
			else 
			{
				tmp->prev->next = tmp->next;
				tmp->next->prev = tmp->prev;
				delete tmp;
				tmp = 0;
				if (x == tail)
				{
					cout << "Not Found!" << endl;
					break;
				}

				y = x;
				x = x->next;
			}
		}
	}
}

bool List::isInList(int el) 
{
	Node *tmp;
	for (tmp = head; tmp != 0 && !(tmp->info == el); tmp = tmp->next);
	return tmp != 0;
}

void List::Cout() 
{
	Node *p = head;
	while (p != NULL) {
		std::cout << p->info;
		p = p->next;
	}
}





















































































































































